package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Memo;
import models.Tag;
import models.User;
import models.services.MemoService;
import models.services.TagService;
import models.services.UserService;
import play.Logger;
import play.data.DynamicForm;
import play.libs.F.Option;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;
import views.html.memo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * メモ画面を管理するコントローラー
 *
 * @author h-ono
 *
 */
public class MemoController extends Controller {

	/**
	 * メモ画面を返却する。
	 *
	 * @return メモ画面
	 */
	public static Result index() {

		String userId = session().get("userId");
		// セッションにユーザIDが保存されていない場合は、ログイン画面を表示
		if (userId == null || userId == "") {
			return ok(login.render());
		}

		return ok(memo.render());
	}

	/**
	 * メモリストを取得する。
	 *
	 * @return メモリストJSON メモリストJSON items:[{ contents:"メモ内容",
	 *         tags:[tag1,tag2,...], user:"投稿者", postDate:"投稿日時" }(取得件数分)]
	 */
	public static Result list() {
		// ajaxのdataを取得
		MemoService memoService = new MemoService();
		List<Option<Memo>> memoList = memoService.findAll();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<ObjectNode> nodeList = new ArrayList<ObjectNode>();
		List tagList = new ArrayList();
		ObjectMapper om = new ObjectMapper();

		for (Option<Memo> result : memoList) {
			ObjectNode memo = Json.newObject();
			List<Tag> tagEntity = Tag.find.where()
					.eq("memo_id", result.get().id).findList();
			for (Tag tag : tagEntity) {
				tagList.add(tag.tag);
			}
			memo.put("contents", result.get().contents);
			memo.put("id", result.get().id);
			memo.put("tags", om.convertValue(tagList, JsonNode.class));
			memo.put("user", result.get().user.name);
			memo.put("postDate", sdf.format(result.get().created));
			nodeList.add(memo);
			tagList.clear();
		}

		return ok(Json.toJson(nodeList));
	}

	/**
	 * タグ検索。
	 *
	 * @return タグ検索結果JSON タグ検索結果JSON items:[{ contents:"メモ内容",
	 *         tags:[tag1,tag2,...], user:"投稿者", postDate:"投稿日時" }(取得件数分)]
	 */
	public static Result tag() {
		return null;
	}

	/**
	 * 投稿されたメモを登録する。登録結果をJSONで返却する。
	 *
	 * @return 登録結果JSON 登録結果JSON {result:"0（正常）または-1（エラー）", msg:"メッセージ"}
	 */
	public static Result post() {

		// ユーザー情報を取得
		UserService userService = new UserService();
		int userId = Integer.parseInt(session().get("userId"));
		Option<User> userInfo = userService.findById(userId);

		// メモ内容を取得
		DynamicForm form = new DynamicForm().bindFromRequest();
		String contents = form.get("contents");
		String tag = form.get("tags");
		String[] tags = new String[0];
		tags = tag.split("\\,");

		// メモ登録情報を登録
		Memo memoInfo = new Memo();
		memoInfo.user = userInfo.get();
		memoInfo.contents = contents;

		MemoService memoService = new MemoService();
		Option<Memo> memoResult = memoService.save(memoInfo);

		// タグ情報を登録
		TagService tagService = new TagService();
		for (int i = 0; i < tags.length; i++) {
			Tag tagInfo = new Tag();
			tagInfo.tag = tags[i];
			tagInfo.memoId = memoInfo.id;
			tagService.save(tagInfo);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (memoResult.isDefined()) {
			ObjectNode result = Json.newObject();
			result.put("result", "0");
			result.put("msg", "登録に成功しました");
			result.put("id", memoInfo.id.toString());
			result.put("user", memoInfo.user.name.toString());
			result.put("postDate", sdf.format(memoInfo.created));
			return ok(result);
		} else {
			flash("error", "入力された内容が登録されませんでした。");
			return ok(memo.render());

		}

		// ajaxのdataを取得
		// try {
		// DynamicForm form = new DynamicForm().bindFromRequest();
		// String userId = form.get("userId");
		// Logger.debug(userId);
		// Logger.debug(form.get("contents"));
		// Logger.debug(form.get("memoId"));
		//
		// //登録結果返却
		// ObjectNode result = Json.newObject();
		// result.put("result", "0");
		// result.put("msg", "登録に成功しました");
		// return ok(result);
		// } catch (Exception e) {
		// // TODO 自動生成された catch ブロック
		// e.printStackTrace();
		// return null;
		// }
	}

	/**
	 * タイムライン上で選択されたメモの詳細情報をJSONで返却する。
	 *
	 * @param id
	 *            メモID
	 * @return 詳細JSON 詳細JSON {contents:"メモ内容", tags:[tag1,tag2,...], user:"投稿者",
	 *         postDate:"投稿日時" }
	 */
	public static Result detail(Integer id) {
		Logger.debug(id.toString());

		// メモ内容を取得する
		MemoService memoService = new MemoService();
		Option<Memo> memoDetail = memoService.findById(id);
		Memo memo = memoDetail.get();
		ObjectNode memoJson = Json.newObject();
		memoJson.put("contents", memo.contents);
		// memoJson.put("tags",memo.tags );
		memoJson.put("user", memo.user.name);
		memoJson.put("postdate", memo.created.toString());
		memoJson.put("winwin", memo.winwin);
		return ok(memoJson);
	}

	/**
	 * メモを削除する。削除結果をJSONで返却する。
	 *
	 * @return 削除結果JSON 削除結果JSON {msg:"メッセージ"}
	 */
	public static Result remove() {
		// メモIDを取得
		DynamicForm form = new DynamicForm().bindFromRequest();
		Integer id = Integer.parseInt(form.get("id"));

		MemoService memoService = new MemoService();
		// 選択されたメモを取得
		Option<Memo> optionMemo = memoService.findById(id);
		if (optionMemo.isDefined()) {
			Memo memo = optionMemo.get();
			// 削除
			memo.delete();

			ObjectNode result = Json.newObject();
			result.put("msg", "メモを削除しました。");
			return ok(result);
		} else {
			flash("error", "メモの削除に失敗しました。");
			return ok(memo.render());
		}
	}

	/**
	 * メモを更新する。更新結果をJSONで返却する。
	 *
	 * @return 更新結果JSON {msg:"メッセージ",
	 *         id:"メモID" user:"投稿者", postDate:"投稿日時" }
	 */
	public static Result update() {

		DynamicForm form = new DynamicForm().bindFromRequest();
		// メモIDを取得
		Integer id = Integer.parseInt(form.get("id"));
		// メモの内容を取得
		String contents = form.get("contents");

		MemoService memoService = new MemoService();
		// 選択されたメモを検索
		Option<Memo> optionMemo = memoService.findById(id);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (optionMemo.isDefined()) {
			Memo memo = optionMemo.get();

			memo.contents = contents;
			memo.created = new Date();

			// メモ更新
			Option<Memo> optionMemoResult = memoService.save(memo);

			if (optionMemoResult.isDefined()) {
				Memo memoResult = optionMemoResult.get();

				ObjectNode result = Json.newObject();
				result.put("msg", "メモを変更しました。");
				result.put("id", memoResult.id.toString());
				result.put("user", memoResult.user.name.toString());
				result.put("postDate", sdf.format(memoResult.created));
				return ok(result);
			}
		}

		flash("error", "メモの変更に失敗しました。");
		return ok(memo.render());
	}


	/**
	 * WinWinカウントをプラスする。
	 *
	 * @return メモJSON 削除結果JSON {winwin:"WinWinカウント"}
	 */
	public static Result plusWinwin(){

		DynamicForm form = new DynamicForm().bindFromRequest();
		// メモIDを取得
		Integer id = Integer.parseInt(form.get("id"));

		MemoService memoService = new MemoService();
		// 選択されたメモを検索
		Option<Memo> optionMemo = memoService.findById(id);

		if (optionMemo.isDefined()) {
			Memo memo = optionMemo.get();
			memo.winwin += 1;

			// メモ更新
			Option<Memo> optionMemoResult = memoService.save(memo);

			if (optionMemoResult.isDefined()) {
				Memo memoResult = optionMemoResult.get();

				ObjectNode result = Json.newObject();
				result.put("winwin", memoResult.winwin);
				return ok(result);
			}
		}

		flash("error", "カウントに失敗しました。");
		return ok(memo.render());
	}

}
