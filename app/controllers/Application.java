package controllers;

import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.User;
import play.*;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
    
    public static Result user(){
    	List<User> users = User.find.all();
    	ObjectNode rootJson = Json.newObject();
    	
    	ObjectNode json = Json.newObject();
    	for(User user : users){
    		json.put("name",user.name);
    		json.put("email", user.email);
    	}
    	rootJson.put("users", json);
    	
    	return ok(rootJson);
    	
    }

}
