package controllers;

import static play.data.Form.form;

import java.util.Date;

import models.User;
import models.services.UserService;
import play.data.Form;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.memo;
import views.html.signup;

/**
 * サインアップ画面を管理するコントローラー
 *
 * @author h-ono
 *
 */
public class SignupController extends Controller {

	/**
	 * Form用の内部クラス
	 */
	public static class SignupForm{

		@Required(message = "名前は必須項目です。")
		@MaxLength(256)
		public String name;

		@Required(message = "メールアドレスは必須項目です。")
		@MaxLength(256)
		@Email(message = "メールアドレスの形式で入力して下さい")
		public String email;

		@Required(message = "パスワードは必須項目です。")
		@MinLength(8)
		@MaxLength(16)
		public String password;
	}

	/**
	 * サインアップ画面を表示する。
	 * @return サインアップ画面
	 */
	public static Result index(){
		return ok(signup.render(new Form(SignupForm.class)));
	}
	/**
	 * 入力されたユーザ情報を登録する。登録結果をJSONで返却する。
	 * @return 登録結果JSON
	 */
	public static Result regist(){

		// 入力データ取得
		Form<SignupForm> f = form(SignupForm.class).bindFromRequest();

		// Formのエラーチェックをしてエラーメッセージを返す
		if(f.hasErrors()){
			return badRequest(signup.render(f));
		}

		SignupForm data = f.get();

		// ユーザの登録処理
		User user = new User();
		user.name = data.name;
		user.email = data.email;
		user.password = data.password;

		Date date = new Date();
		user.created = date;
		user.updated = date;

		// サービス情報の取得
		UserService userService = new UserService();
		Option<User> userInfo = userService.save(user);

		//メモ画面へ遷移
		session("userId",userInfo.get().id.toString());
		session("userName",userInfo.get().name.toString());
		return ok(memo.render());
	}
}
