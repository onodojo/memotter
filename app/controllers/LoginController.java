package controllers;

import models.User;
import models.services.UserService;
import play.data.DynamicForm;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;
import views.html.memo;

/**
 * ログイン画面を管理するコントローラー
 *
 * @author h-ono
 *
 */
public class LoginController extends Controller {
	/**
	 * ログイン画面を返却する。
	 * @return ログイン画面
	 */
	public static Result index(){
		return ok(login.render());
	}

	/**
	 * 入力されたアカウントで認証を行う。
	 * @return 認証OKの場合はメモ画面にリダイレクト、認証NGの場合はログイン画面にエラーメッセージ
	 */
	public static Result auth(){

		//ユーザーIDとパスワードを取得
		DynamicForm form =  new DynamicForm().bindFromRequest();
		String userId = form.get("userId");
		String password = form.get("password");

		//ユーザー情報を取得
		UserService userService = new UserService();
		Option<User> userInfo = userService.findByAuth(userId,password);

		if(userInfo.isDefined()){
		//ユーザー認証OKならメモ画面へ遷移
			session("userId",userInfo.get().id.toString());
			session("userName",userInfo.get().name.toString());
			return ok(memo.render());
		}else{
		//ユーザー認証NGならエラーメッセージを表示
			flash("error","入力された内容が正しくありません。");
			return ok(login.render());
		}

	}
}
