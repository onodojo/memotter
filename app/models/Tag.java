package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

/*
 * タグ情報Bean
 *
 * @author Shingo
 *
 */
@Entity
@Table(name = "t_tag")
public class Tag extends Model {
	private static final long serialVersionUID = 1L;
	@Id
	public Long id;
	@Nonnull
	public String tag;
	public Long memoId;
	@CreatedTimestamp
	public Date created;
	public Date updated;
	public Date deleted;


	public static Finder<Long, Tag> find = new Finder<Long, Tag>(Long.class , Tag.class);

}
