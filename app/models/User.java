package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

/**
 * ユーザ情報Bean
 *
 * @author Shinsuke
 *
 */
@Entity
@Table(name="m_user")
public class User extends Model {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	public Long id;
	@Nonnull
	public String name;
	@Column(unique=true,nullable=false)
	public String email;
	@Nonnull
	public String password;
	@CreatedTimestamp
	public Date created;
	public Date updated;
	public Date deleted;

	public static Finder<Long,User> find = new Finder<Long,User>(Long.class,User.class);

}
