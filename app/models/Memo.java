package models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

/**
 * メモ情報
 * @author ninomiya
 *
 */
@Entity
@Table(name = "t_memo")
public class Memo extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Id
	public Long id;
	@Nonnull
	public String contents;
	public long winwin;

	//public String[] tags;
	//@Nonnull
	//public String userId;
	@CreatedTimestamp
	public Date created;
	public Date updated;
	public Date deleted;

	@ManyToOne
	public User user;


	public static Finder<Long, Memo> find = new Finder<Long, Memo>(Long.class, Memo.class);

}
