package models.services;

import java.util.ArrayList;
import java.util.List;

import models.Tag;
import play.libs.F;
import play.libs.F.Option;

/**
 * メモ管理サービス
 * @author ninomiya
 *
 */
public class TagService implements ModelService<Tag> {

	/*
	 * (非 Javadoc)
	 * メモ情報ID検索
	 * @see models.services.ModelService#findById(java.lang.Integer)
	 */
	@Override
	public Option<Tag> findById(Integer id) {
		Tag tag = Tag.find.byId(id.longValue());
		return apply(tag);
	}

	/*
	 * (非 Javadoc)
	 * メモ情報登録
	 * @see models.services.ModelService#save(play.db.ebean.Model)
	 */
	@Override
	public Option<Tag> save(Tag entry) {
		Option<Tag> optionTag = apply(entry);
		if(optionTag.isDefined()){
			entry.save();
		}

		return optionTag;
	}

	/*
	 * (非 Javadoc)
	 * メモ情報全件検索
	 * @see models.services.ModelService#findAll()
	 */
	@Override
	public List<Option<Tag>> findAll() {
		List<Tag> TagList = Tag.find.all();
		List<Option<Tag>> optionList = new ArrayList<Option<Tag>>();

		// オプション詰め替え
		for(Tag Tag:TagList){
			Option<Tag> optionTag = apply(Tag);
			optionList.add(optionTag);
		}

		return optionList;
	}

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }

}
