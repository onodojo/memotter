package models.services;

import java.util.ArrayList;
import java.util.List;

import models.User;
import play.libs.F;
import play.libs.F.Option;

import com.avaje.ebean.Query;

/**
 * ユーザ情報サービスクラス
 * 
 * @author Shinsuke
 *
 */
public class UserService implements ModelService<User> {

	/* (非 Javadoc)
	 * ユーザ情報ID検索
	 * @see models.services.ModelService#findAll()
	 */
	@Override
	public Option<User> findById(Integer id) {
		User user = User.find.byId(id.longValue());
		return apply(user);
	}

	/* (非 Javadoc)
	 * ユーザ情報登録
	 * @see models.services.ModelService#findAll()
	 */
	@Override
	public Option<User> save(User entry) {
		Option<User> optionUser = apply(entry);
		optionUser.get().save();
		return optionUser;
	}

	/* (非 Javadoc)
	 * ユーザ情報全件検索
	 * @see models.services.ModelService#findAll()
	 */
	@Override
	public List<Option<User>> findAll() {
		//全件検索
		List<User> userList = User.find.all();
		List<Option<User>> optionList = new ArrayList<Option<User>>();
		
		//オプション詰め替え
		for(User user:userList){
			Option<User> optionUser = apply(user);
			optionList.add(optionUser);
		}
		
		return optionList;
	}
	
	/**
	 * Eメールとパスワードに一致するユーザを返却します。
	 * 
	 * @param email Eメール
	 * @param password パスワード
	 * @return ユーザ情報
	 */
	public Option<User> findByAuth(String email,String password){
		//Eメール、パスワードチェック
		if(isEmpty(email) || isEmpty(password)){
			//Eメール,パスワードが不正
			return null;
		}
		
		//検索条件
		// TODO SQLインジェクション対策(エスケープ文字など)
		StringBuilder sb = new StringBuilder("email='");
		sb.append(email);
		sb.append("' And password='");
		sb.append(password);
		sb.append("'");
		Query<User> query = User.find.where(sb.toString());

		//検索
		return apply(query.findUnique());
	}
	
	
	
	/**
	 * 文字列のnull,空文字チェック
	 * true :nullまたは空文字
	 * false:nullまたは空文字でない
	 * 
	 * @param str　判定対象文字列
	 * @return 判定結果
	 */
	private boolean isEmpty(String str){
		if(str == null || str.isEmpty()){
			return true;
		}
		return false;
	}
	
	/**
	 * Optionラップ
	 * 
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }
}
