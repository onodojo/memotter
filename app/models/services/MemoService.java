package models.services;

import java.util.ArrayList;
import java.util.List;

import models.Memo;
import play.libs.F;
import play.libs.F.Option;

/**
 * メモ管理サービス
 * @author ninomiya
 *
 */
public class MemoService implements ModelService<Memo> {

	/*
	 * (非 Javadoc)
	 * メモ情報ID検索
	 * @see models.services.ModelService#findById(java.lang.Integer)
	 */
	@Override
	public Option<Memo> findById(Integer id) {
		Memo memo = Memo.find.byId(id.longValue());
		return apply(memo);
	}

	/*
	 * (非 Javadoc)
	 * メモ情報登録
	 * @see models.services.ModelService#save(play.db.ebean.Model)
	 */
	@Override
	public Option<Memo> save(Memo entry) {
		Option<Memo> optionMemo = apply(entry);
		if(optionMemo.isDefined()){
			entry.save();
		}

		return optionMemo;
	}

	/*
	 * (非 Javadoc)
	 * メモ情報全件検索
	 * @see models.services.ModelService#findAll()
	 */
	@Override
	public List<Option<Memo>> findAll() {
		List<Memo> memoList = Memo.find.all();
		List<Option<Memo>> optionList = new ArrayList<Option<Memo>>();

		// オプション詰め替え
		for(Memo memo:memoList){
			Option<Memo> optionMemo = apply(memo);
			optionList.add(optionMemo);
		}

		return optionList;
	}

	/**
	 * Optionラップ
	 *
	 * @param value ラップ対象
	 * @return
	 */
	private <A> F.Option<A> apply(A value) {
        if(value != null) {
            return F.Option.Some(value);
        } else {
            return F.Option.None();
        }
    }

}
