/**
 *メモデータ取得・登録ajax通信
 */

/*
 * メモデータ全件取得
 * 非同期通信でメモデータを全件取得する
 *
 * リクエストJSONデータ
 * {}
 *
 * レスポンスJSONデータ
 * items:[{
 *          id:"メモID",
 * 			contents:"メモ内容",
 * 			tags:[tag1,tag2,...],
 * 			user:"投稿者",
 * 			postDate:"投稿日時"
 * }(取得件数分)]
 */
function getMemoAll(){
	//非同期通信
	$.ajax({
        type     : 'get',
        url      : '/memotter/list',
        dataType : 'json',
        cache    : false,
        success  : function(data, status, jqXHR) {
	        //メモリスト描画(ループ)
	        //var memoList = JSON.parse(data['items']);
        	var memoList = data;
//	        Object.keys(memoList).forEach(function (key) {
//	        	var memo = memoList[key];
	        	//TODO リストに表示する文字の切り抜き暫定対応
//	        	memo['contents'] = memo['contents'].substr(0,10)
//	        	putMemoList(memo)
//	        });
	        for (i = 0; i < memoList.length; i++) {
	        	var memo = memoList[i];
	        	//TODO リストに表示する文字の切り抜き暫定対応
	        	memo['contents'] = memo['contents'].substr(0,10);
	        	putMemoList(memo);
	        }
        	resetEvent();
        },
        error: function(jqXHR, status, error) {
            alert('通信に失敗');
        }
    });
}

/*
 * メモデータ取得(タグ検索)
 * 非同期通信でメモデータを全件取得する
 *
 * リクエストJSONデータ
 * {}
 *
 * レスポンスJSONデータ
 * items:[{
 *          id:"メモID",
 * 			contents:"メモ内容",
 * 			tags:[tag1,tag2,...],
 * 			user:"投稿者",
 * 			postDate:"投稿日時"
 * }(取得件数分)]
 */
function getMemoTag(){
    alert('タグ検索');
}

/*
 * メモ詳細データ取得(メモ参照)
 * 非同期通信でメモ詳細データを取得する
 *
 * リクエストJSONデータ
 * {}
 *
 * レスポンスJSONデータ
 * {contents:"メモ内容",
 * 	tags:[tag1,tag2,...],
 * 	user:"投稿者",
 * 	postDate:"投稿日時" }
 */
function getMemoDetail(memoid){
	//非同期通信
	$.ajax({
        type     : 'get',
        url      : '/memotter/'+memoid,
        dataType : 'json',
        cache    : false,
        success  : function(data, status, jqXHR) {
        	//前の詳細情報を削除
        	$('#memodetail .modal-content').remove();

        	//メモ詳細表示
        	var detailmemo = '<div class="modal-content">';
        	detailmemo += '<h4>メモ内容</h4>';
        	detailmemo += '<br>';
        	detailmemo += '<textarea class="materialize-textarea" id="memoUpdate">';
        	detailmemo += data['contents'];
        	detailmemo += '</textarea>';
        	detailmemo += '<input type="hidden" id="selectedId" value="' + memoid + '" >';
        	detailmemo += '</div>';

        	//詳細情報を追加
        	$('#memodetail').prepend(detailmemo);

        	$('.winwin-cnt').text(data['winwin']);
        },
        error: function(jqXHR, status, error) {
            alert('通信に失敗');
        }
    });
}

/*
 * メモ投稿
 * 非同期通信で投稿されたメモの登録を行う
 *
 * リクエストJSONデータ
 * {contents:"メモ内容",
 * 	tags:[tag1,tag2,...]"}
 *
 * レスポンスJSONデータ
 * {result:"0（正常）または-1（エラー）",
 * 	msg:"メッセージ"}
 */
function postMemo(){
    var contents = $('#memoAdd').val();
    var tag = $('#tagAdd').val();
	//非同期通信
	$.ajax({
        type     : 'post',
        url      : '/memotter/post',
        data     : {contents : contents, tags : tag},
        dataType : 'json',
        cache    : false,
        success  : function(data, status, jqXHR) {
	        //alert('登録結果:' + data['result']);
	        alert('登録メッセージ:' + data['msg']);
	        //TODO メモID取得、メモデータJSONのデータを作る。
	        contents = contents.substr(0,10)
	        var memodata = {id: data['id'],
			        		contents: contents,
			        		tags: tag,
			        		user: data['user'],
			        		postDate: data['postDate']}
	        //登録結果反映→登録データのみ反映
        	putMemoList(memodata)
        	resetEvent();

	        // 入力フォームクリア
	        $('#memoAdd').val("");
	        $('#tagAdd').val("");
	        // ダイアログを閉じる
	        $('#modal1').closeModal();
        },
        error: function(jqXHR, status, error) {
            alert('通信に失敗');
        }
    });
}

/*
 * メモデータ反映
 * 引数のメモデータをリストの末尾に追加する。
 *
 * memodata(JSON)
 * {id:"メモID",
 * 	contents:"メモ内容",
 * 	tags:[tag1,tag2,...],
 * 	user:"投稿者",
 * 	postDate:"投稿日時"}
 *
 */
function putMemoList(memodata){
		//リストに追加するメモ情報の作成
		var putmemo = '<li id="';
		putmemo += memodata['id'];
		//putmemo += '" class = "collection-item avatar">';
		putmemo += '" class = "collection-item">';
		putmemo += ' <img src="/assets/images/memo_icon.jpg" alt="" class="circle"/>';
		putmemo += ' <span class="title">';
		putmemo += memodata['user'];
		putmemo += ' ' ;
		putmemo += memodata['postDate'];
		putmemo += ' </span>';
		putmemo += '<p class="contents">';
		putmemo += memodata['contents'];
		putmemo += '</p>';
		putmemo += '<p class="tags">';
		putmemo += memodata['tags'];
		putmemo += '</p>';
		putmemo += '</li>';

		//リストの先頭に追加
//		$('#memoId').append(putmemo);
//		$('#memoId').prepend(putmemo);
		$('.collection-header').after(putmemo);
}

/*
 * リストイベント再設定
 * リスト要素追加時に
 * リストクリック時の処理を再設定する。
 *
 */
function resetEvent(){
	//リスト要素のイベント再設定
	$(document).on("click", "li.collection-item", function(){
        $("#memodetail").openModal();
        getMemoDetail($(this).attr('id'));
	});
}

/*
 * メモ削除
 * 非同期通信で選択されたメモの削除を行う
 *
 * リクエストJSONデータ
 * {id:"メモID"}
 *
 * レスポンスJSONデータ
 * {msg:"メッセージ"}
 */
function deleteMemo(){
	//TODO 削除権限のあるユーザか判定が必要
	if(confirm("削除しますか？")){
		// 選択されたメモのIDを取得
		var id = $('#selectedId').val();

		//非同期通信
		$.ajax({
	        type     : 'post',
	        url      : '/memotter/remove',
	        data     : {id : id},
	        dataType : 'json',
	        cache    : false,
	        success  : function(data, status, jqXHR) {
	        	alert(data['msg']);
	        	// 削除したメモを画面から削除
		        $("#" + id).remove();
	        },
	        error: function(jqXHR, status, error) {
	            alert('通信に失敗');
	        }
	    });
	}
}

 /*
* メモ変更
* 非同期通信で選択されたメモの変更を行う
*
* リクエストJSONデータ
*  {id:"メモID",
* 	contents:"メモ内容"}
*
* レスポンスJSONデータ
*  {msg:"メッセージ",
* 	id:"メモID"
* 	user:"投稿者",
* 	postDate:"投稿日時" }
*/
function updateMemo(){
	//TODO 更新権限のあるユーザか判定が必要
	if(confirm("変更しますか？")){
		// 選択されたメモのIDを取得
		var id = $('#selectedId').val();
		// メモの内容を取得
		var contents = $('#memoUpdate').val();

		// タグ（暫定対応：詳細ダイアログではなく一覧側のタグを取得）
		var tag = $("#" + id).find('.tags').text();

		//非同期通信
		$.ajax({
	        type     : 'post',
	        url      : '/memotter/update',
	        data     : {id : id,
	        			contents: contents},
	        dataType : 'json',
	        cache    : false,
	        success  : function(data, status, jqXHR) {
	        	alert(data['msg']);

	        	// 変更前のメモを削除
	        	$("#" + id).remove();

	        	var memodata = {id: data['id'],
		        		contents: contents,
		        		tags: tag,
		        		user: data['user'],
		        		postDate: data['postDate']}
		        //変更後のメモを先頭に追加
		    	putMemoList(memodata);

	        },
	        error: function(jqXHR, status, error) {
	            alert('通信に失敗');
	        }
	    });
	}
}


/*
 * ページ読み込み（画面初期表示処理）
 */
window.onload = function()
{
	getMemoAll();
};

/*
 * 画面のjavascriptを移植
 */
var list = null;

$(function() {
	/*
	 * モーダルの表示
	 */
	$("a[href='#modal1']").click(function() {
        $('#modal1').openModal();
    });

    $("li.collection-item").click(function(){
       list = $(this);
       $("#memodetail").openModal();
    });

     var interval = 2000;

    $(document).on("li.collection-item", "touchstart", function()
            {
               timer = setTimeout( function()
            {
               alert( "画像の保存はできません" );
            }, interval );
     });

    /**
     * Positions the tabs
     */

	function positionTabs(){
		var tabs = $('#tab-wrapper');

		if(tabs.length){
			var nav = $('#nav');
			var search = $('#search');
			var content = $('#content');
			var offsetTop = window.pageYOffset || document.documentElement.scrollTop;

			if(offsetTop > (nav.height() + (search.is(':visible') ? search.height() : 0)) && window.innerWidth > 600){
					tabs.addClass('fixed');
					content.css({'padding-top': (tabs.height() + parseInt(tabs.css('margin-bottom'))) + 'px'});
			}else{
					tabs.removeClass('fixed');
					content.css({'padding-top': 0});
			}
		}
	}

	$('#toggle-search').click(function(){
	 var search = $('#search');
	 if(search.css("display") == "none"){
		 search.slideDown();
	 }else{
		 search.slideUp();
	 }

	$('#delete').click(function(){
	   list.remove();
	 });
	});


	/*
	 * WinWinボタン押下
	 */
	$('.winwin-btn').click(function(){

		// 選択されたメモのIDを取得
		var id = $('#selectedId').val();

		//非同期通信
		$.ajax({
	        type     : 'post',
	        url      : '/memotter/winwin',
	        data     : {id : id},
	        dataType : 'json',
	        cache    : false,
	        success  : function(data, status, jqXHR) {

	        	$('.winwin-cnt').text(data['winwin']);

	        },
	        error: function(jqXHR, status, error) {
	            alert('通信に失敗');
	        }
	    });

	});
});

